<?php


namespace Sibertec\LightspeedLeads\Interfaces;


/**
 * Interface IIntegrationResponse
 *
 * @property IErrorMessage Error
 * @property IProspectResult|IProspectResult[] Prospect
 *
 * @package Interfaces
 */
interface IIntegrationResponse
{
}
