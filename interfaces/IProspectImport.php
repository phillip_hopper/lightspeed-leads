<?php


namespace Sibertec\LightspeedLeads\Interfaces;


/**
 * Interface IProspectImport
 *
 * @property string SourceProspectId
 * @property string DealershipId
 * @property string Via
 * @property string Email
 * @property string Name
 * @property string Phone
 * @property string AltPhone
 * @property string SourceDate
 * @property string Address1
 * @property string Address2
 * @property string City
 * @property string State
 * @property string ZipCode
 * @property string Country
 * @property string DLNumber
 * @property string SSN
 * @property string Gender
 * @property string VehicleNewUsed
 * @property string VehicleType
 * @property string VehicleMake
 * @property string VehicleModel
 * @property string VehicleYear
 * @property string VehicleColor
 * @property string VehicleVIN
 * @property string VehicleStockNum
 * @property string VehiclePrice
 * @property string ItemSKU
 * @property string ItemSupplierCode
 * @property string ItemDescription
 * @property string ItemPrice
 * @property string ItemLaborRate
 * @property string ItemLaborHours
 * @property string Notes
 * @property string ProspectType
 * @property string BirthDate
 * @property string TradeVehicleYear
 * @property string TradeVehicleMake
 * @property string TradeVehicleModel
 * @property string TradeOdometer
 * @property string TradeColor
 * @property string TradePayoff
 * @property string PurchaseTimeframe
 *
 * @package Interfaces
 */
interface IProspectImport
{
}
