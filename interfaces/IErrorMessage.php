<?php


namespace Sibertec\LightspeedLeads\Interfaces;


/**
 * Interface IErrorMessage
 *
 * @property string Code
 * @property string Message
 *
 * @package Interfaces
 */
interface IErrorMessage
{
}
