<?php


namespace Sibertec\LightspeedLeads\Interfaces;


/**
 * Interface IProspectResult
 *
 * @property string SourceProspectId
 * @property string PCHId
 * @property string DestinationDealerType
 *
 * @package Interfaces
 */
interface IProspectResult
{
}
