<?php /** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedLeads\Authentication;
use Sibertec\LightspeedLeads\Curl;

class WebServiceTest extends TestCase
{
    public function testWebService()
    {
        $this->markTestSkipped();

        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $url = 'http://pch.v-sept.com/VSEPTPCHPostService.aspx?method=AddProspect&sourceid=' . $auth->SourceID;

        $xml = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
  <AddProspect xmlns="http://tempuri.org/">
    <prospectXml>
      <ProspectImport>
        <Item>
          <SourceProspectId>1</SourceProspectId>
          <DealershipId>{$auth->DealerID}</DealershipId>
          <Email>johndoe@example.com</Email>
          <Name>John Doe</Name>
          <Phone>555-555-5555</Phone>
        </Item>
        <Item>
          <SourceProspectId>2</SourceProspectId>
          <DealershipId>{$auth->DealerID}</DealershipId>
          <Email>janedoe@example.com</Email>
          <Name>Jane Doe</Name>
          <Phone>666-666-6666</Phone>
        </Item>
      </ProspectImport>
    </prospectXml>
    <sourceId>{$auth->SourceID}</sourceId>
  </AddProspect>
</soap:Body>
</soap:Envelope>
XML;

        $response = Curl::PostXml($url, $xml, $auth->ApiKey);

        $this->assertNotEmpty($response);
    }
}
