<?php /** @noinspection PhpUnhandledExceptionInspection */


use PHPUnit\Framework\TestCase;
use Sibertec\LightspeedLeads\Authentication;
use Sibertec\LightspeedLeads\Interfaces\IProspectImport;
use Sibertec\LightspeedLeads\ProspectImporter;

class ProspectImporterTest  extends TestCase
{
    public function testSendProspects()
    {
        $this->markTestSkipped();

        $auth = Authentication::LoadFromYaml(__DIR__ . '/config/lightspeed_data.testing.yml');

        $pi = new ProspectImporter($auth);

        /** @var IProspectImport $p */
        $p = new stdClass();
        $p->SourceProspectId = 'RVW001';
        $p->Name = 'Prospect One';
        $p->Phone = '9378439001';

        $pi->QueueProspect($p);
        $this->assertNotEmpty($pi->GetQueue());
        $this->assertEquals(1, count($pi->GetQueue()));

        $p = new stdClass();
        $p->SourceProspectId = 'RVW002';
        $p->Name = 'Prospect Two';
        $p->Phone = '9378439002';

        $pi->QueueProspect($p);
        $this->assertEquals(2, count($pi->GetQueue()));

        $results = $pi->SendProspects();
        $this->assertTrue(is_array($results));
    }
}
