# Sibertec\Lightspeed-Leads

A small, easy to use PHP library for sending leads and quotes to Lightspeed through the CDK Global Recreation CEM Lead Integration API.

#### For more usage samples, look in the [tests](./tests) directory.

#### Format of yaml settings file, if you choose to use one:
```yaml
DealerID: "12345678"           # for LightspeedADP API
SourceID: "leadsourceexample"  # for Lead Integration XML web service
ApiKey: "your-api-key"         # for Lead Integration XML web service
```
