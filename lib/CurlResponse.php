<?php declare(strict_types=1);


namespace Sibertec\LightspeedLeads;


use Sibertec\LightspeedLeads\Interfaces\IErrorMessage;
use Sibertec\LightspeedLeads\Interfaces\IIntegrationResponse;
use SimpleXMLElement;

abstract class CurlResponse
{
    /** @var IErrorMessage */
    public $Error;

    /** @var IIntegrationResponse */
    protected $response;

    /**
     * CurlResponse constructor.
     *
     * @param SimpleXMLElement $xml
     */
    public function __construct(SimpleXMLElement $xml)
    {
        // convert the SimpleXMLElement to a stdClass
        $this->response = json_decode(json_encode($xml));

        if (!empty($this->response->Error))
            $this->Error = $this->response->Error;
    }
}
