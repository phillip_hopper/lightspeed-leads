<?php declare(strict_types=1);


namespace Sibertec\LightspeedLeads;


use Exception;

class Curl
{
    /**
     * Submit the request as XML using POST
     * @param string $url
     * @param string $xml
     * @param string $api_key
     * @param string $action
     *
     * @return AddProspectResults|IntegrationStatusMessage
     * @throws Exception
     */
    public static function PostXml($url, $xml, $api_key, $action='http://tempuri.org/AddProspect')
    {
        $headers = array(
            'Cache-Control: no-cache',
            'Connection: Keep-Alive',
            'X-PCHAPIKey: ' . $api_key,
            'SOAPAction: ' . $action,
            'Content-Type: text/xml'
        );

        $params = [CURLOPT_POST => 1, CURLOPT_POSTFIELDS => $xml];

        $response = self::doCurl($url, $headers, $params);

        // parse the xml
        $xml = simplexml_load_string($response);

        // what kind of response did we receive
        if (strpos($response, '<VSEPTIntegrationStatusMessage') === 0) {
            $out = new IntegrationStatusMessage($xml);
        }
        elseif (strpos($response, '<AddProspectResults') === 0) {
            $out = new AddProspectResults($xml);
        }
        else {
            throw new Exception('Unexpected response from web service:' . PHP_EOL . PHP_EOL . $response . PHP_EOL);
        }

        if (!empty($out->Error))
            throw new Exception('Error from XML web service:' . PHP_EOL . PHP_EOL . $out->Error->Message . PHP_EOL);

        return $out;
    }

    /**
     * Execute the request using curl
     *
     * @param $url
     * @param $headers
     * @param array|null $params
     *
     * @return mixed
     * @throws Exception
     */
    private static function doCurl($url, $headers, $params = null)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,60); // 60 second connection timeout
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);       // 5 minute function timeout
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (!empty($params)) {
            foreach($params as $key => $value) {
                curl_setopt($ch, $key, $value);
            }
        }

        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (!empty($err))
            throw new Exception($errmsg);

        if ($http_code > 399)
            throw new Exception('HTTP error ' . $http_code . ': ' . $content, $http_code);

        return $content;
    }
}
