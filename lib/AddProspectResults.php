<?php declare(strict_types=1);


namespace Sibertec\LightspeedLeads;


use Sibertec\LightspeedLeads\Interfaces\IProspectResult;
use SimpleXMLElement;

class AddProspectResults extends CurlResponse
{
    /** @var IProspectResult[] */
    public $Prospects = [];

    /**
     * AddProspectResults constructor.
     *
     * @param SimpleXMLElement $xml
     */
    public function __construct(SimpleXMLElement $xml)
    {
        parent::__construct($xml);

        // check for added prospects
        if (!empty($this->response->Prospect)) {
            if (is_array($this->response->Prospect)) {
                foreach($this->response->Prospect as $prospect) {
                    $this->Prospects[] = $prospect;
                }
            }
            else {
                $this->Prospects[] = $this->response->Prospect;
            }
        }
    }
}
