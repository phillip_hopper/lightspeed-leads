<?php declare(strict_types=1);


namespace Sibertec\LightspeedLeads;


use SimpleXMLElement;

class IntegrationStatusMessage extends CurlResponse
{
    /**
     * IntegrationStatusMessage constructor.
     *
     * @param SimpleXMLElement $xml
     */
    public function __construct(SimpleXMLElement $xml)
    {
        parent::__construct($xml);
    }
}
