<?php declare(strict_types=1);

namespace Sibertec\LightspeedLeads;


use Symfony\Component\Yaml\Yaml;

class Authentication
{
    /** @var string */
    public $DealerID;

    /** @var string */
    public $SourceID;

    /** @var string */
    public $ApiKey;

    /**
     * Authentication constructor.
     *
     * @param string $dealer_id
     * @param string $source_id
     * @param string $api_key
     */
    public function __construct($dealer_id, $source_id, $api_key)
    {
        $this->DealerID = $dealer_id;
        $this->SourceID = $source_id;
        $this->ApiKey   = $api_key;
    }

    /**
     * Get an Authentication object initialized from the specified file.
     *
     * @param string $yaml_file_name
     *
     * @return Authentication
     */
    public static function LoadFromYaml($yaml_file_name)
    {
        $settings = Yaml::parseFile($yaml_file_name);
        return new Authentication($settings['DealerID'], $settings['SourceID'], $settings['ApiKey']);
    }
}
