<?php /** @noinspection PhpUnused */
declare(strict_types=1);


namespace Sibertec\LightspeedLeads;


use Exception;
use Sibertec\LightspeedLeads\Interfaces\IProspectImport;
use Sibertec\LightspeedLeads\Interfaces\IProspectResult;
use SimpleXMLElement;

class ProspectImporter
{
    /** @var Authentication */
    private $authentication;

    /** @var IProspectImport[] */
    private $prospects = [];

    const soap_template = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
  <AddProspect xmlns="http://tempuri.org/">
    <prospectXml>
      <ProspectImport/>
    </prospectXml>
  </AddProspect>
</soap:Body>
</soap:Envelope>
XML;

    /**
     * ProspectImporter constructor.
     *
     * @param Authentication $auth
     */
    public function __construct($auth)
    {
        $this->authentication = $auth;
    }

    /**
     * Performs a required fields check and adds to the queue
     *
     * Required Fields:
     * - SourceProspectId
     * - Name
     * - Email or Phone
     *
     * @param IProspectImport $prospect
     *
     * @throws RequiredValueException
     */
    public function QueueProspect($prospect)
    {
        // check required fields
        if (empty($prospect->SourceProspectId))
            throw new RequiredValueException('Prospect SourceProspectId is a required field.');

        if (empty($prospect->Name))
            throw new RequiredValueException('Prospect Name is a required field.');

        if (empty($prospect->Email) && empty($prospect->Phone))
            throw new RequiredValueException('Either Email or Phone is required for a Prospect.');

        $this->prospects[] = $prospect;
    }

    /**
     * This is included for unit testing purposes
     * @return IProspectImport[]
     */
    public function GetQueue()
    {
        return $this->prospects;
    }

    /**
     * Send the queue of new prospects in batches to the Prospect Clearing House API
     *
     * @return IProspectResult[]
     * @throws Exception
     */
    public function SendProspects()
    {
        if (empty($this->prospects))
            return [];

        $count = 0;
        $return_val = [];

        /** @var SimpleXMLElement $xml */
        $xml = $this->GetNewXmlDoc();
        $prospects = $xml->xpath('//_:ProspectImport')[0];

        // send in batches of 20
        foreach($this->prospects as $prospect) {

            $prospect->DealershipId = $this->authentication->DealerID;

            $count++;

            // create a new node
            $item = $prospects->addChild('Item');

            // add the values that are set
            $vars = get_object_vars($prospect);
            foreach($vars as $key => $val) {
                if (empty($val))
                    continue;

                $item->addChild($key, $val);
            }

            // when we get to 20, send the batch
            if ($count > 19) {

                $return_val = array_merge($return_val, $this->ProcessBatch($this->authentication, $xml));

                $count = 0;
                $xml = $this->GetNewXmlDoc();
                $prospects = $xml->xpath('//_:ProspectImport')[0];
            }
        }

        // if there are any left over, process them now
        if ($count > 0)
            $return_val = array_merge($return_val, $this->ProcessBatch($this->authentication, $xml));

        return $return_val;
    }

    /**
     * Send the SOAP request containing prospects to be added
     *
     * @param Authentication $auth
     * @param SimpleXMLElement $xml
     *
     * @return IProspectResult[]
     * @throws Exception
     */
    private static function ProcessBatch($auth, $xml)
    {
        $url = 'http://pch.v-sept.com/VSEPTPCHPostService.aspx?method=AddProspect&sourceid=' . $auth->SourceID;

        /** @var AddProspectResults $result */
        $result = Curl::PostXml($url, $xml->asXML(), '');

        if (get_class($result) != 'Sibertec\LightspeedLeads\AddProspectResults')
            throw new Exception('Type of $result was not AddProspectResults.');

        return $result->Prospects;
    }

    /**
     * Creates a new XML document and initializes XPath
     *
     * @return SimpleXMLElement
     */
    private function GetNewXmlDoc()
    {
        // create a new XML document
        $xml = new SimpleXMLElement(self::soap_template);

        // initialize XPath with the namespaces from the document
        $ns = $xml->getNamespaces(true);
        foreach ($ns as $key => $val) {

            // alias the sub-namespace with a blank name as '_', otherwise XPath won't work
            $xml->registerXPathNamespace($key ?: '_', $val);
        }

        // set the sourceId
        $xml->xpath('//_:AddProspect')[0]->addChild('sourceId', $this->authentication->SourceID);

        return $xml;
    }
}
